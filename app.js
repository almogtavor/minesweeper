import { BombsMap } from './BombsMap';


window.onload = function loadFunc() {
	defineUpperButtons();
	defineInstructions();

	face.addEventListener("click", reset);

	var board = document.getElementById("board");

	board.style.height = height * 55 + "px";
	board.style.width = width * 55 + "px";

	defineGlobalVariables();
	const nearButtonsPlaces = [[-1,-1],[-1,0],[-1,1],[0,-1],[0,1],[1,-1],[1,0],[1,1]]

	for (let i = 0; i < height; i++) {
		block[i] = new Array(width);
		for (let j = 0; j < width; j++) {
			blockDefinition(i, j, rightClick, leftClick);
			board.appendChild(block[i][j]);
		}
		if (i === height - 1) {
			// const b1 = new BombsMap(mines, height, width, block);
		}
	}

	function rightClick(event)
	{
		if (!gameEnded) {
			coordinate_i = event.target.coordinate_i;
			coordinate_j = event.target.coordinate_j;
			event.preventDefault();
			flagPlacing();
			flagsCounter();
			winCheck();
		}
	}


	function leftClick(event, coordinate_i = event.target.coordinate_i, coordinate_j = event.target.coordinate_j)
	{
		if (!gameEnded) {
			if (timerInterval === null) {
				start();
			}
			if (block[coordinate_i][coordinate_j].src.substr(-8,) !="flag.png")
			{
				if(block[coordinate_i][coordinate_j].isBomb === true)
				{
					bombCase(coordinate_i, coordinate_j);
				}
				else if (block[coordinate_i][coordinate_j].nearBombsNum > 0)
				{
					bombsNearBlockCase(coordinate_i, coordinate_j);
				}
				else {
					blankBlockCase(coordinate_i, coordinate_j, nearButtonsPlaces, leftClick, event);
				}
			}
		}
	}

	function reset() {
		defineInstructions();

		if (height * width < mines)
			alert("Instructions error. Please checkout there are less mines than height * width.")
		else {
			stop();
			resetVariables();
			loadFunc()
		}
	}	
}

function defineGlobalVariables() {
	gameEnded = false;
	correctFlagsNum = 0;
	flagsNum = 0;
	timerInterval = null;
	block = new Array(height);
}

function changeValue() {
	value++;
	if (value < 10) {
		timerRight.src = "time" + (value) + ".png";
	}
	else if (value < 100) {
		timerRight.src = "time" + (value % 10) + ".png";
		timerMiddle.src = "time" + (Math.floor(value / 10)) + ".png";
	}
	else if (value < 1000 ) {
		timerRight.src = "time" + (value % 10) + ".png";
		timerMiddle.src = "time" + (Math.floor(value / 10) % 10) + ".png";
		timerLeft.src = "time" + (Math.floor(value / 100)) + ".png";
	}
	else {
		face.src = "lose.png";
		gameEnded = true;
		stop();
	}
}

function start() {
	value = 0;
	timerInterval = setInterval(changeValue, 1000);  
}

function stop() {
	clearInterval(timerInterval);
}

function blockDefinition(i, j, rightClick, leftClick) {
	block[i][j] = document.createElement("img");
	block[i][j].src = "empty-block.png";
	block[i][j].coordinate_i = i;
	block[i][j].coordinate_j = j;
	block[i][j].isBomb = false;
	block[i][j].nearBombsNum = 0;
	block[i][j].addEventListener("contextmenu", rightClick);
	block[i][j].addEventListener("click", leftClick);
}

function flagsCounter() {
	flagRight.src = "time" + (flagsNum % 10) + ".png";
	flagMiddle.src = "time" + (Math.floor(flagsNum / 10) % 10) + ".png";
}

function winCheck() {
	if (mines == flagsNum && mines == correctFlagsNum) {
		gameEnded = true;
		stop();
		face.src = "win.png";
	}
}

function flagPlacing() {
	if (block[coordinate_i][coordinate_j].src.substr(-15) === "empty-block.png" && flagsNum != mines) {
		flagMarking();
	}
	else if (block[coordinate_i][coordinate_j].src.substr(-8) === "flag.png") {
		flagErasing();
	}
}

function flagErasing() {
	block[coordinate_i][coordinate_j].src = "empty-block.png";
	flagsNum--;
	if (block[coordinate_i][coordinate_j].isBomb === true) {
		correctFlagsNum--;
	}
}

function flagMarking() {
	block[coordinate_i][coordinate_j].src = "flag.png";
	flagsNum++;
	if (block[coordinate_i][coordinate_j].isBomb === true) {
		correctFlagsNum++;
	}
}

function resetVariables() {
	timerInterval = null;
	timerLeft.src = "time0.png";
	timerMiddle.src = "time0.png";
	timerRight.src = "time0.png";
	flagLeft.src = "time0.png";
	flagMiddle.src = "time0.png";
	flagRight.src = "time0.png";
	face.src = "smile.png";
	document.getElementById("board").innerHTML = "";
}

function defineInstructions() {
	heightElement = document.getElementById("height");
	height = heightElement.options[heightElement.selectedIndex].text;
	widthElement = document.getElementById("width");
	width = widthElement.options[widthElement.selectedIndex].text;
	minesElement = document.getElementById("mines");
	mines = minesElement.options[minesElement.selectedIndex].text;
}

function defineUpperButtons() {
	flagLeft = document.getElementById("flag-left");
	flagMiddle = document.getElementById("flag-middle");
	flagRight = document.getElementById("flag-right");
	face = document.getElementById("face");
	timerLeft = document.getElementById("timer-left");
	timerMiddle = document.getElementById("timer-middle");
	timerRight = document.getElementById("timer-right");
}

function bombCase(coordinate_i, coordinate_j) {
	face.src = "lose.png";
	gameEnded = true;
	stop();
	for (var i = 0; i < height; i++) {
		for (var j = 0; j < width; j++) {
			if (i === coordinate_i && j === coordinate_j) {
				block[coordinate_i][coordinate_j].src = "bomb-at-clicked-block.png";
			}
			else if (block[i][j].isBomb === true) {
				block[i][j].src = "unclicked-bomb.png";
			}
			else if (block[i][j].src.substr(-8) === "flag.png") {
				block[i][j].src = "wrong-flag.png";
			}
		}
	}
}

function bombsNearBlockCase(coordinate_i, coordinate_j) {
	block[coordinate_i][coordinate_j].src = block[coordinate_i][coordinate_j].nearBombsNum + ".png";
}

function blankBlockCase(coordinate_i, coordinate_j, nearButtonsPlaces, leftClick, event) {
	block[coordinate_i][coordinate_j].src = "blank.png";
	nearButtonsCheck(nearButtonsPlaces, coordinate_i, coordinate_j, leftClick, event);
}

function nearButtonsCheck(nearButtonsPlaces, coordinate_i, coordinate_j, leftClick, event) {
	for (const [x, y] of nearButtonsPlaces) {
		const i = coordinate_i + x;
		const j = coordinate_j + y;
		if (i >= 0 && j >= 0 && i < height && j < width) {
			if (!(block[i][j].isBomb) && block[i][j].src.substr(-15) === "empty-block.png") {
				if (block[i][j].nearBombsNum === 0) {
					leftClick(event, i, j);
				}
				else {
					block[i][j].src = block[i][j].nearBombsNum + ".png";
				}
			}
		}
	}
}


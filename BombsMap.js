class BombsMap {
    constructor (mines, height, width, block) {
        this.mines = mines;
        this.height = height;
        this.width = width;
        this.block = block;
    }

    get bombsPlacing() {
        var bombPlaced;
		for (var i = 0; i < this.mines; i++) {
			bombPlaced = false
			while (bombPlaced === false) {
				var rnd_i = Math.floor(Math.random() * this.height);
				var rnd_j = Math.floor(Math.random() * this.width);
				if (this.block[rnd_i][rnd_j].isBomb === false)
				{
					this.block[rnd_i][rnd_j].isBomb = true;
					bombPlaced = true
					nearBombUpdate(rnd_i, rnd_j);
				}
			}
        }
        return this.block;
    }
    
    static nearBombUpdate(rnd_i, rnd_j) {
        for (var m = 0; m < this.height; m++) {
            for (var n = 0; n < this.width; n++) {
                if (Math.abs(rnd_i - m) <= 1 && Math.abs(rnd_j - n) <= 1)
                    this.block[m][n].nearBombsNum++;
            }
        }
    }
}

export {BombsMap};